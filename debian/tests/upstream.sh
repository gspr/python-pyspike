#!/bin/bash

set -e
set -u

cp -R $PWD/test $AUTOPKGTEST_TMP
cd $AUTOPKGTEST_TMP

for py3ver in $(py3versions -vs)
do
    echo "Running upstream tests with Python ${py3ver}."
    /usr/bin/python${py3ver} -B -m pytest -v test
done
