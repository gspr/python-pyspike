python-pyspike (0.8.0+dfsg-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add runtime dependency on python3-pkg-resources (Closes: #1093236)

 -- Alexandre Detiste <tchet@debian.org>  Mon, 20 Jan 2025 08:15:15 +0100

python-pyspike (0.8.0+dfsg-3) unstable; urgency=medium

  * Add patch to support Cython 3 and Python 3.13. (Closes: #1087645)
    - Thanks to s3v <c0llapsed@yahoo.it> for the hint and for testing.
    - Build-depend on cython3 again, instead of -legacy.
  * Add patch to use raw strings for docstrings that contain TeX macros.
    (Closes: #1086951)
  * Standards-version 4.7.0.1. No changes needed.

 -- Gard Spreemann <gspr@nonempty.org>  Tue, 03 Dec 2024 16:34:01 +0100

python-pyspike (0.8.0+dfsg-2) unstable; urgency=medium

  * Build-depend on cython3-legacy instead of cython3. (Closes: #1056868)

 -- Gard Spreemann <gspr@nonempty.org>  Sat, 02 Dec 2023 12:29:25 +0100

python-pyspike (0.8.0+dfsg-1) unstable; urgency=medium

  * New upstream version.
    - Drop all patches, no longer needed.
  * Introduced DFSG repack to drop generated file erroneously shipped by
    upstream.
  * Adapt d/watch to DFSG version suffix.
  * Disable build-time tests due to them relying on absolute file
    paths. Autopkgtests are still available.
  * Add patch to stop installing generated C sources.

 -- Gard Spreemann <gspr@nonempty.org>  Sun, 26 Nov 2023 15:15:00 +0100

python-pyspike (0.7.0-2) unstable; urgency=medium

  * Add patch to stop using deprecated NumPy types. (Closes: #1027238)
  * Add patch to replace Nose with PyTest. (Closes: #1018568)
  * Build-depend on PyTest and SciPy for tests, and have autopkgtests do
    the same.
  * Standards-version 4.6.2. No changes needed.

 -- Gard Spreemann <gspr@nonempty.org>  Mon, 02 Jan 2023 22:39:41 +0100

python-pyspike (0.7.0-1) unstable; urgency=medium

  * New upstream version.
  * Drop upstreamed patches.

 -- Gard Spreemann <gspr@nonempty.org>  Thu, 30 Dec 2021 17:45:53 +0100

python-pyspike (0.6.0-6) unstable; urgency=medium

  * Call dh_numpy3.

 -- Gard Spreemann <gspr@nonempty.org>  Sat, 27 Nov 2021 16:52:29 +0100

python-pyspike (0.6.0-5) unstable; urgency=medium

  * Add patch to fix Python 3.10 compatibility.

 -- Gard Spreemann <gspr@nonempty.org>  Sun, 21 Nov 2021 15:14:28 +0100

python-pyspike (0.6.0-4) unstable; urgency=medium

  * Fix broken watch file.
  * Correct typo in upstream/metadata file.
  * Standards-version 4.6.0.1. No changes needed.

 -- Gard Spreemann <gspr@nonempty.org>  Fri, 15 Oct 2021 11:02:13 +0200

python-pyspike (0.6.0-3) unstable; urgency=medium

  * Move git repository to Salsa.
  * DH compat 13.
  * Start upstream/metadata file.
  * Standards-version 4.5.1. No changes needed.

 -- Gard Spreemann <gspr@nonempty.org>  Sat, 23 Jan 2021 18:55:01 +0100

python-pyspike (0.6.0-2) unstable; urgency=medium

  * Add patch to allow floating point behavior differences in tests.

 -- Gard Spreemann <gspr@nonempty.org>  Sat, 07 Mar 2020 18:53:50 +0100

python-pyspike (0.6.0-1) unstable; urgency=medium

  * Initial release. (Closes: #932726)

 -- Gard Spreemann <gspr@nonempty.org>  Thu, 06 Feb 2020 09:47:07 +0100
